package com.so206.po.ext;

import com.so206.po.SystemDomain;

public class DomainCheckExtQuery extends SystemDomain {

    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
